<?php

namespace App\Http\Middleware;

use Closure;
use \App\Profile;

class CustomServiceProvider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private static function getProfile(&$request,&$graph = null){

        if(!self::dev()){
            $service_provider_id = $graph->getId();
        }else{
            $service_provider_id = 123;
        }
        return Profile::where('service_provider_id',$service_provider_id)->first();
    }
    private static function dev(){
        return env('APP_DEBUG')==true;
    }
    public function handle($request, Closure $next)
    {   
        if($request->is('*/projects/share*'))return $next($request);
        $pass = false;
        $service_provider_response = null;
        $error_code = 200;
        if(!self::dev()){
          $app_secret = '1757555e94227045490cee7090458885';
          $app_id = '728135517275940';
          $appsecret_proof= hash_hmac('sha256', $request->header('X-ACCESS-TOKEN'), $app_secret);
          $app_token = file_get_contents("https://graph.facebook.com/oauth/access_token?client_id=$app_id&client_secret=$app_secret&grant_type=client_credentials"); 
          $app_token = json_decode($app_token,true);
          $res = ["message"=>"success"];
          $fb = new \Facebook\Facebook([
            'app_id' => $app_id,
            'app_secret' => $app_secret,
            'app_secret_proof' => $app_token["access_token"],
            'appsecret_proof' => $app_token["access_token"],
            'default_graph_version' => 'v3.3',
            //'default_access_token' => '{access-token}', // optional
          ]);

          // Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
          //   $helper = $fb->getRedirectLoginHelper();
          //   $helper = $fb->getJavaScriptHelper();
          //   $helper = $fb->getCanvasHelper();
          //   $helper = $fb->getPageTabHelper();

          try {
              // Get the \Facebook\GraphNodes\GraphUser object for the current user.
              // If you provided a 'default_access_token', the '{access-token}' is optional.
              $service_provider_response = $fb->get('/me', $request->header('X-ACCESS-TOKEN'));
              $me = $service_provider_response->getGraphUser();
              $builder = Profile::where('service_provider_id',$me->getId());
              if($builder->count()<=0){
                $model = new Profile;
                $model->service_provider_id = $me->getId();
                $model->name = $me->getName();
                $model->save();
              }
              $profile = self::getProfile($request,$me);
              if(!$profile){
                $res["message"] = 'user-not-found';
                $error_code = 404;
              }else{
                $request->merge([
                  "service_provider_graph"=>$me,
                  "user_profile"=>$profile,
                ]);
                $pass = true;
              }
          } catch(\Facebook\Exceptions\FacebookResponseException $e) {
              // When Graph returns an error
              $res["message"] = $e->getMessage();
              $res["X-ACCESS-TOKEN"] = $request->header('X-ACCESS-TOKEN');
              $error_code = 500;
          } catch(\Facebook\Exceptions\FacebookSDKException $e) {
              // When validation fails or other local issues
              $res["message"] = $e->getMessage();
              $res["X-ACCESS-TOKEN"] = $request->header('X-ACCESS-TOKEN');
              $error_code = 400;
          }
        }else{
          $pass = true;
          $profile = self::getProfile($request);
          $request->merge([
            "user_profile"=>$profile,
          ]);
        }
        if(!$pass)return \Response::json($res, $error_code);
        return $next($request);
    }
}
