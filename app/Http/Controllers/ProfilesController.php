<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfilesController extends Controller
{
    //
    public function check_provider(Request $request){
    	return response()->json(["profile"=>
    		[
    			'name'=>$request->service_provider_graph->getName(),
    			'firstname'=>$request->service_provider_graph->getFirstName(),
    		]
    	]); 
    }
}
