<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Project;
use \App\Profile;

class ProjectsController extends Controller
{
    //
    const MAX_PROJECTS = 30;
    const MAX_DATA_SIZE = 100000;
    private static function dev(){
        return env('APP_DEBUG')==true;
    }
    private static function &fillData(&$request,&$model){ 
        $model->name = !empty($request->name) ? $request->name : (empty($model->name) ? 'empty' : $model->name) ;    
        if( $request->data ) {
            $model->data = $request->data;
        }    
        if( $request->has( 'shared' ) ) {
            $model->shared = (int)$request->shared;
        }    
        if( $request->has( 'background_color' ) ) {
            $model->background_color = $request->background_color;
        }    
        if(self::dev())$model->profile_id = 1;
        if( !$model->hash_identifier ) $model->hash_identifier = hash('md5', mt_rand() . mt_rand() );
        return $model;
    }
    public function list(Request $request){
        $profile = $request->user_profile;
        $data = Project::where('profile_id',$profile->id)->get();
    	return response()->json($data);
    }
    public function list_shared(Request $request){
        $data = Project::where('shared',1)->orderby('updated_at','DESC')->get();
        foreach ($data as $key => &$record) {
            unset($record->id,$record->profile_id);
        }
        return response()->json($data);
    }
    public function create(Request $request)
    { 
        $profile = $request->user_profile;
        if($profile->projects->count()>self::MAX_PROJECTS)return response()->json(["msg"=>"too-many-projects","max"=>30],400);
        $record = new Project;
        if($record){
            $record = &self::fillData($request,$record);
            $record->profile_id = $profile->id;
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"cant-create"],500);
        }
    }
    public function update( Request $request, $id )
    { 
        if( is_string($request->data) && strlen( $request->data ) > self::MAX_DATA_SIZE ){
            return response()->json( [ "msg" => "data-too-long" ] , 400 );
        }
        $profile = $request->user_profile;
        $record = Project::where( 'id' , $id )->where( 'profile_id' , $profile->id )->first();
        if( $record) {
            $record = &self::fillData( $request , $record );
            if( $record->save() ){
                return response()->json( $record ); 
            }else{
                return response()->json( [ "msg" => "cant-save" ] , 400 );
            }
            
        }else{
            return response()->json( [ "msg" => "not-found" ] , 404 );
        }
    } 
    public function share( Request $request , $share_id )
    { 
        $record = Project::where('hash_identifier',$share_id)->first();
        if($record){
            if( $request->v == '2' ){
                return response()->json([
                    'data' => $record->data,
                    'background_color' => $record->background_color,
                ]); 
            }else{
            return response()->json($record->data); 
            }
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    } 
    public function destroy(Request $request,$id)
    {
        $profile = $request->user_profile;
        $data = Project::where( 'id' , $id )->where( 'profile_id' , $profile->id )->first();
        if($data){
        	if(self::dev() && $id==1)return response()->json(["msg"=>"cant-remove-test-project"],404);
            try{
                if($data->delete()){
                    return response()->json($data); 
                }else{
                    return response()->json(["msg"=>"cant-delete"],400);
                }
            }catch(\Illuminate\Database\QueryException $e){
                return response()->json(["msg"=>"cant-delete"],500);
            } 
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
}
