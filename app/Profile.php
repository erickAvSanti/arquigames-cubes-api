<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;
class Profile extends Model
{
    //
    protected $fillable = [
    	'service_provider',
    	'service_provider_id',
    	'name',
    ];
    public static function list_projects($id){
    	$profile = Profile::find($id);
    	$profile->projects;
    	return $profile;
    }
    public function projects(){
        return $this->hasMany(Project::class,'profile_id','id');
    }
}
