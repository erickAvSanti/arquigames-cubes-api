<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;

class Project extends Model
{
    //
    public function profile()
	{
	    return $this->belongsTo(Profile::class,'profile_id','id');
	}
}
