<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::group(['middleware' => ['cors','custom_service_provider']], function () {

	Route::prefix('cubes')->group(function () { 
		Route::post('/check_provider', 'ProfilesController@check_provider'); 
		Route::prefix('projects')->group(function () { 
			$regex_id_int = '^[\d]+$';
	    Route::get('/', 'ProjectsController@list'); 
	    Route::get('/share/{share_id}', 'ProjectsController@share'); 
	    Route::put('/{id}', 'ProjectsController@update')->where(['id'=>$regex_id_int]);
	    Route::post('/', 'ProjectsController@create'); 
	    Route::delete('/{id}', 'ProjectsController@destroy')->where(['id'=>$regex_id_int]); 
		
		});
	
	});
});
Route::group(['middleware' => ['cors']], function () {

	Route::prefix('cubes')->group(function () { 
		Route::prefix('projects')->group(function () { 
	    Route::post('/shared', 'ProjectsController@list_shared'); 
		});
	
	});
});

/*
Route::get('/',function(){
	return response()->json(["message"=>"success_from_get"]); 
});
Route::post('/',function(){
	return response()->json(["message"=>"success_from_post"]); 
});
*/